package com.kvn.reflect;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by wangzhiyuan on 2018/8/13
 */
public class AccessClassLoader extends ClassLoader {
    private static AccessClassLoader accessClassLoader = new AccessClassLoader();

    private AccessClassLoader() {
        super(Thread.currentThread().getContextClassLoader());
    }

    public static AccessClassLoader instance(){
        return accessClassLoader;
    }

    /**
     * 定义类
     * @param name
     * @param data
     * @return
     */
    public Class<?> defineClassForName(String name, byte[] data) {
        try {
            // 类已经定义过，直接加载
            return this.loadClass(name);
        } catch (ClassNotFoundException e) {
            // 类没有定义过，动态生成
            return this.defineClass(name, data, 0, data.length);
        }
    }


    /**
     * 将.class文件输出到d:/
     *
     * @param className 类全限定名。如：com.cn.zsy.Main
     * @param bytes      字节码数组
     */
    public static void output2File(String className, byte[] bytes) {
        String fileName = className.substring(className.lastIndexOf(".") + 1);
        String dest = "d:/" + fileName + ".class";
        System.out.println("className = [" + className + "], dest : " + dest);

        try (FileChannel channel = new FileOutputStream(dest).getChannel()) {
            channel.write(ByteBuffer.wrap(bytes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 输出字节码到控制台
     * @warn 看源码实现，它是基于 class 文件存在的情况来输出的
     * @param clazz 类的全限定名
     */
    public static void outputBytecode(Class clazz) {
        try {
            jdk.internal.org.objectweb.asm.ClassReader cr = new jdk.internal.org.objectweb.asm.ClassReader(clazz.getName());
            jdk.internal.org.objectweb.asm.util.TraceClassVisitor visitor = new jdk.internal.org.objectweb.asm.util.TraceClassVisitor(new PrintWriter(System.out));
            cr.accept(visitor, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
