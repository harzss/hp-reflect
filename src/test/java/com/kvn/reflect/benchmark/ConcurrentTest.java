package com.kvn.reflect.benchmark;

import com.kvn.reflect.MethodAccess;

import java.util.stream.IntStream;

/**
 * Created by wangzhiyuan on 2019/1/29
 */
public class ConcurrentTest {
    private static ConcurrentTest target = new ConcurrentTest();

    public void m1(){
        System.out.println("m1:" + this);
    }

    public static void main(String[] args) {
        Runnable task = () -> {
            MethodAccess.get(ConcurrentTest.class).invoke(target, "m1");
        };

        IntStream.range(1, 3).forEach((int i) -> {
            Thread t = new Thread(task);
            t.start();
        });


    }
}
