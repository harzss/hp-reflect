package com.kvn.reflect;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

/**
 * Created by wangzhiyuan on 2018/8/14
 */
public class FieldAccessTest {

    @Test
    public void get() {
        FieldAccess fieldAccess = FieldAccess.get(FooService.class);
        FooService instance = new FooService();
        fieldAccess.set(instance, "stringParam1", "abc");
        fieldAccess.set(instance, "intParam2", 123);
        fieldAccess.set(instance, "point", new Point(2,3));
        System.out.println(instance);
        System.out.println(fieldAccess.get(instance, "stringParam1"));
        System.out.println(fieldAccess.get(instance, "intParam2"));
        System.out.println(fieldAccess.get(instance, "point"));
    }
}