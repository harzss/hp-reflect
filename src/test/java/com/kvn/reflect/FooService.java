package com.kvn.reflect;

import java.awt.*;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wangzhiyuan on 2018/8/13
 */
public class FooService {
    private String stringParam1;
    protected int intParam2;
    public Point point;

    private String method1(){
        sum();
        return "method1";
    }

    private String method11(String param1){
        sum();
        return "method11" + param1;
    }

    private void sum() {
        int sum = 0;
        for (int i = 0; i < 100; i++) {
            sum += Math.sin(i);
        }
    }

    protected String[] method2(String param1) {
        sum();
        return new String[]{"method2", "param1:" + param1};
    }
    public String[] method22(String[] param1) {
        sum();
        return new String[]{"method22", "param1:" + param1};
    }

    public List method3(String param1, int[] param2){
        sum();
        List ls = new ArrayList();
        ls.add("method3");
        ls.add(param1);
        ls.add(param2);
        return ls;
    }

    public String getStringParam1() {
        return stringParam1;
    }

    public void setStringParam1(String stringParam1) {
        this.stringParam1 = stringParam1;
    }

    public int getIntParam2() {
        return intParam2;
    }

    public void setIntParam2(int intParam2) {
        this.intParam2 = intParam2;
    }

    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }
}
